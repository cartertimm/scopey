# Run as py ./setup.py py2exe

from distutils.core import setup
import py2exe

options = {'py2exe': {
            'bundle_files': 1,
            'includes': ['pyvisa', 'pyvisa.resources.resource']
            }
          }

setup(console=['scopey.py'], options=options)
