
# scopey.py
# Carter Timm

# Scopey - a helpful tool for managing USB-connected oscilloscopes.

# Screenshot manager for VISA-enabled oscilloscopes (most modern scopes). Currently only works with
# Tektronix oscilloscopes connected to your system via USB.

# Usage: python -u scopey

# Tested under Python 3.11, may work for earlier Python versions.
# Required modules: psutil, pyusb, pyvisa, pyvisa-py, zeroconf

from pathlib import Path
import sys

import pyvisa


VERSION = "1.1"


# Print the main menu
def help():
    print()
    print('Valid commands:')
    print()
    print('    ls [path]')
    print()
    print('By default, lists files and subdirectories under the current working directory as shown')
    print('in the command prompt. Subdirectories are denoted with a trailing "/". If a path is')
    print('supplied, lists subdirectories in the supplied path. See `cd` below for more details.')
    print('Both absolute and relative paths are supported. Note that all directories are printed')
    print('last so that they are closer to the command prompt and therefore easier to see. A blank')
    print('line separates files and directories to further aid navigation.')
    print()
    print()
    print('    cd [path]')
    print()
    print('Set the directory path where images should be saved. Paths may be specified as absolute')
    print('(e.g. `cd c:/users/you`) or relative (e.g. `cd myDir`) to the current working path.')
    print('Server locations (e.g. "//server/path") are not supported. Use ".." to go up a level in')
    print('the directory structure. Spaces in paths must not be escaped - simply type them in')
    print('normally (e.g. `cd dir1/another dir/end_dir`). Running `cd` with no arguments will')
    print('return you to the directory that you ran this program from initially. Running `cd -`')
    print('will return you to the previous directory you were in.')
    print()
    print()
    print('    cap <filename>')
    print()
    print('Get an oscilloscope screen capture and save to the specified filename at the current')
    print('working path. Filenames will have ".png" appended to them if the filename does not')
    print('already end in ".png".')
    print()
    print()
    print('    name <channel> [label]')
    print()
    print('Set an oscilloscope channel label. `channel` should be specified as an integer. `label`')
    print('can contain spaces. Do not escape these spaces, simply enter them normally, similar to')
    print('what is described above in `cd`. If `label` is not present, the label will be cleared')
    print('on the scope. Note that this feature may not be available for all scopes, in which case')
    print('Scopey will simply do nothing if this command is run.')
    print()
    print()
    print('    reconnect')
    print()
    print('Reconnect to either the currently-connected or a different oscilloscope.')
    print()
    print()
    print('    which')
    print()
    print('Report the make and model of the oscilloscope that Scopey is currently connnected to.')
    print('Note that Scopey only checks connection with the scope when it needs to send commands')
    print('to the scope, so this in reality this command only returns what Scopey thinks it is')
    print('connected to.')
    print()
    print()
    print('    quit')
    print()
    print('Quit this program.')
    print()
    print()
    print('    exit')
    print()
    print('Same as `quit`. Can also do Ctrl-C.')
    print()
    print()
    print('    help')
    print()
    print('Print this command list again.')
    print()
    print()
    print('    h')
    print()
    print('Same as `help`.')
    print()
    print()
    print('    ?')
    print()
    print('Same as `help`.')
    print()
    print('Commands are case-sensitive.')
    

# Helper function to handle path resolution for ls() and cd()
def createNewPath(path, currentPath):
    
    # Check if the new path is absolute
    path = path.strip()
    newPath = None
    if len(path) > 2 and (path[2] == ':' or path[:2] == '//' or path[:2] == '\\\\'):
        newPath = Path(path)
    
    # Otherwise add it on to the current path
    else:
        newPath = currentPath / path
        
    # Make sure the new path exists, otherwise report
    if newPath.exists() and newPath.is_dir():
        return newPath
    else:
        print(f'ERROR: {newPath.resolve()} is not a directory!')
        return None
        

# Print out all files and subdirectories under the current working path
def ls(path, currentPath):
    
    # Only try to form a new path if it was supplied, otherwise we'll just use the current working
    # directory
    lsPath = currentPath
    if path is not None:
        newPath = createNewPath(path, currentPath)
        if newPath is not None:
            lsPath = newPath
        else:
            return
    
    # Get all files in the path, appending a trailing '/' if it is a directory
    filesAndDirs = [f.parts[-1] + '/' if f.is_dir() else f.parts[-1] for f in lsPath.iterdir()]
    filesAndDirs.sort()
    filenames = [filename for filename in filesAndDirs if filename[-1] != '/']
    dirnames = [dirname for dirname in filesAndDirs if dirname[-1] == '/']
    for filename in filenames:
        print(filename)
    print()
    for dirname in dirnames:
        print(dirname)
        
        
# Change the current working path
def cd(path, currentPath, lastPath, initialPath):
    
    # If run with no arguments, set currentPath to initialPath
    if path is None:
        lastPath = currentPath
        currentPath = initialPath
        return currentPath, lastPath
        
    # Check if we should simply change to the last path we were in
    if path == '-':
        tmp = lastPath
        lastPath = currentPath
        currentPath = tmp
        return currentPath, lastPath
    
    # Form the new path, only use it if it was valid
    newPath = createNewPath(path, currentPath)
    if newPath is not None:
        lastPath = currentPath
        currentPath = newPath
        
    return currentPath, lastPath
    

# Save a scope capture    
def cap(scope, filename, currentPath, scopeDescription):

    # Ensure that the scope is connected
    if scope is None:
        print('ERROR: Not connected to an oscilloscope!')
        return scope, scopeDescription
    
    # Ensure that there was a filename supplied
    if filename is None:
        print('ERROR: No filename was supplied!')
        return scope, scopeDescription
    
    # Append '.png' if necessary
    if filename [-4:] != '.png':
        filename += '.png'
    
    filepath = currentPath / filename
    
    # Check before overwriting
    if filepath.exists():
        response = input(f'{filename} already exists. Overwrite? y/[N]: ')
        if len(response) == 0 or response[0].lower() != 'y':
            return scope, scopeDescription
            
    # Get and save the capture, making sure that we are still connected to the scope and updating if
    # not
    try:
        scope.write('SAVE:IMAGE:FILEFORMAT PNG')
        scope.write('HARDCOPY:PORT USB')
        scope.write('HARDCOPY:INKSAVER OFF')
        scope.write('HARDCOPY START')
        scopeCap = scope.read_raw()
        with open(filepath, 'wb') as outfile:
            outfile.write(scopeCap)
        
    except:
        print(f'ERROR: {scopeDescription} no longer connected!')
        scope = None
        scopeDescription = '<<< NOT CONNECTED >>>'
    
    return scope, scopeDescription
        
        
# Name a scope channel - currently supports up to 8-channel oscilloscopes
def name(scope, argument, scopeDescription):

    # Ensure that the scope is connected
    if scope is None:
        print('ERROR: Not connected to an oscilloscope!')
        return scope, scopeDescription
    
    # Ensure that we have arguments to work with
    if argument is None:
        print('ERROR: No arguments supplied to `name`!')
        return scope, scopeDescription
    
    # Split the arguments since the `argument` parameter currently contains both the channel and
    # label
    splitArgs = argument.split(maxsplit=1)
    
    # Get the channel
    channel = None
    try:
        channel = int(splitArgs[0])
    except ValueError:
        print(f'ERROR: "{splitArgs[0]}" is not a valid channel!')
        return scope, scopeDescription
        
    # Channel indices should always be 1 or greater
    if channel < 1:
        print(f'ERROR: "{channel}" is not a valid channel!')
        
    # Set up the label
    label = ''
    if len(splitArgs) > 1:
        label = splitArgs[1]
        
    # Set the label, making sure that we are still connected to the scope and updating if not
    try:
        scope.write("CH" + str(channel) + ":LABEL '" + label.replace("'", '') + "'")
        
    except:
        print(f'ERROR: {scopeDescription} no longer connected!')
        scope = None
        scopeDescription = '<<< NOT CONNECTED >>>'
    
    return scope, scopeDescription
    

# Helper function used by main() and init() to connect to an oscilloscope
def connectToEquipment(shouldExit):

    # Report to user
    print('Detecting USB-connected test equipment...')

    # Get a handle to the VISA resource manager and get all resources connected via USB
    manager = pyvisa.ResourceManager()
    resources = [resource for resource in manager.list_resources() if str(resource)[:3] == 'USB']
    
    # Report and exit if no test equipment found
    if len(resources) == 0:
        print('ERROR: No USB-connected test equipment found!')
        
        if shouldExit:
            print('Exiting...')
            sys.exit(-1)
        
        else:
            return None, '<<< NOT CONNECTED >>>'

    # Get a list of all identities as returned by the connected test equipment
    equipment = [manager.open_resource(resource).query('*IDN?').strip() for resource in resources]
    
    # Print what test equipment is present
    print()
    for idx, description in enumerate(equipment):
        print(f'{idx}: {description}')
    print()
    
    # Form the prompt to the user
    prompt = f'Select the oscilloscope to connect to ([0]): '
    if len(equipment) > 1:
        prompt = f'Select the oscilloscope to connect to ([0]-{len(equipment)-1}): '
    
    # Prompt user to select what they want to connect to, using try/except block to catch errors if
    # the user did not enter an integer
    selection = None
    try:
        raw_selection = input(prompt)
        if raw_selection == '':
            selection = 0
        else:
            selection = int(raw_selection)
    except ValueError:
        print(f'ERROR: "{raw_selection}" is not a valid selection! Defaulting to 0...')
        selection = 0
    
    # Use 0 as default if invalid index supplied
    if selection < 0 or selection >= len(equipment):
        print(f'ERROR: "{raw_selection}" is not a valid selection! Defaulting to 0...')
        selection = 0
    
    # Get a handle to the oscilloscope
    scope = manager.open_resource(resources[selection])
    scope.timeout = 10000
    
    # Report to user
    make, model = equipment[selection].split(',')[:2]
    scopeDescription = f'{make} {model}'
    print(f'Connected to {scopeDescription}')
    
    return scope, scopeDescription


# Initialize the oscilloscope and paths
def init():

    # Connect to the scope
    scope, scopeDescription = connectToEquipment(True)
    
    # Set initial and current paths
    initialPath = Path('.')
    currentPath = initialPath
    lastPath = currentPath
    
    return scope, scopeDescription, currentPath, lastPath, initialPath
    

def main():

    # Print version
    global VERSION
    print(f'Scopey v{VERSION}')

    # Initialize the oscilloscope
    scope, scopeDescription, currentPath, lastPath, initialPath = init()
    
    # Report how to get a command list
    print('Type `help` for a list of commands.')
        
    # Main running loop
    while True:
        
        # Prompt
        userInput = input(f'SCOPEY >>> {currentPath.resolve()} $ ').strip()
        
        # Skip the case where the user just entered whitespace
        if userInput == '':
            continue
        
        # Split command from argument - the argument is assumed to be everything after the command
        # at first - also handle the case where there is just a command with no argument
        splitInput = userInput.split(maxsplit=1)
        command = splitInput[0]
        argument = None
        if len(splitInput) > 1:
            argument = splitInput[1]
            
        # Switch based on command
        if command == 'ls':
            ls(argument, currentPath)
            
        elif command == 'cd':
            currentPath, lastPath = cd(argument, currentPath, lastPath, initialPath)
            
        elif command == 'cap':
            scope, scopeDescription = cap(scope, argument, currentPath, scopeDescription)
            
        elif command == 'name':
            scope, scopeDescription = name(scope, argument, scopeDescription)
            
        elif command == 'reconnect':
            scope, scopeDescription = connectToEquipment(False)
            
        elif command == 'which':
            print(scopeDescription)
            
        elif command in ('quit', 'exit'):
            break
            
        elif command in ('h', 'help', '?'):
            help()
        
        else:
            print(f'ERROR: Command `{command}` not known.')
            
    # No cleanup is necessary


if __name__ == '__main__':

    # Run in try-except block to gracefully exit from Ctrl-C without printing a traceback
    try:
        main()
    except KeyboardInterrupt:
        pass
    
