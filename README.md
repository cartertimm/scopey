# Scopey

The simple oscilloscope screenshot tool


## Description

Welcome to Scopey. Scopey is designed to be a simple oscilloscope screenshot manager for scopes connected to your
machine via USB. Currently, Scopey only works with Tektronix scopes, but is easy enough to extend (I only have Tek scopes to
test with).

Currently tested with the following scopes:
- MDO34
- DPO2024
- TDS2024B

This project was born out of the lack of open-source oscilloscope screenshot managers. All I needed was to be able to
efficiently save screenshots and change channel labels without scrolling through every letter in the oscilloscope
"keyboard". Scopey solves these issues by providing a simple built-in command line interface to cover both of these
tasks.

Once you run Scopey, enter `help` to get a list of all supported commands with descriptions and usage.

Currently only tested under Windows. I am not, nor do I claim to be, a Windows developer, so your mileage may vary with
this. On Windows, you will need to install NI-VISA, available from ni.com.

Running `python setup.py py2exe` will generate a "dist" folder with an executable and libraries necessary to run as
an exe. This makes things simpler for distribution as users do not need to have Python installed on their machine to
run Scopey. However, users will still need to install NI-VISA.

Again, this has not been tested under Linux (yet...), but note that Linux will give you trouble with accessing USB
devices unless you run Scopey as root. This can be avoided by setting appropriate udev rules.

Tested with python 3.11. It is very likely that earlier versions will work just fine.

## Python Dependencies

- pyvisa
- psutil (to get rid of error messages from pyvisa)
- zeroconf (to get rid of error messages from pyvisa)

## Other Dependencies

- NI-VISA ([Download](https://www.ni.com/en/support/downloads/drivers/download.ni-visa.html))
-- Note that you won't need to install any of the additional options
